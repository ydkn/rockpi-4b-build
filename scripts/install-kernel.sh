#!/bin/bash -e

ARCHIVE_PATH=$(dirname "${0}")

if [ -z "${1}" ]; then
  echo "usage: ${0} <boot-partition-device>"
  exit 1
fi

if ! [ -f "${1}" ]; then
  echo "device '${1}' does not exist"
  exit 1
fi

echo "Installing boot partition..."
dd if="${ARCHIVE_PATH}/boot.img" of="${1}"

echo "Installing modules..."
tar -xzf "${ARCHIVE_PATH}/modules.tar.gz" -C /

echo "Installing headers..."
tar -xzf "${ARCHIVE_PATH}/headers.tar.gz" -C /

if [ -x "$(command -v dkms)" ]; then
  echo "Building DKMS modules..."

  dkms autoinstall -k $(cat "${ARCHIVE_PATH}/VERSION")
else
  echo "DKMS not installed. Skipping module installation!"
fi
