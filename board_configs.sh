#!/bin/bash -e

export BOARD="rockpi4b"
export ARCH="arm64"
export DEFCONFIG="rockchip_linux_defconfig"
export UBOOT_DEFCONFIG="rock-pi-4b-rk3399_defconfig"
export DTB="rockpi-4b-linux.dtb"
export CROSS_COMPILE="aarch64-linux-gnu-"
export CHIP="rk3399"
export DEBIAN_ARCH="arm64"
